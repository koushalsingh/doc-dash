import React from 'react';
import { BrowserRouter, Link } from 'react-router-dom';
import './App.css';
import Routers from './components/MyRoutes/Routers';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-circular-progressbar/dist/styles.css';

function App() {
   return (
      <div className="App">

         <BrowserRouter>

            <Routers></Routers>
         </BrowserRouter>


      </div>
   );
}

export default App;
