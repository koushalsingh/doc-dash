import React, { Fragment, useState } from 'react'
import BillingIcon from '../icons/Billing.svg'

import TotalPaitent from '../icons/TotalPaitent.svg'
import CalanderDarkIcon from '../icons/CalanderDarkIcon.svg'
import PendingTask from '../icons/PendingTask.svg'
import Waiting from '../icons/Waiting.svg'

import './Card.css'
import WaitingPatiens from '../WaitingPatients/WaitingPatiens'
import { CircularProgressbarWithChildren } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';


const Card = () => {

   const [lgShow, setLgShow] = useState(false);
   const percentage = 12;
   const Appointmentpercentage = 80;
   const Patientspercentage = 10;
   const PendingTaskpercentage = 60;
   return (
      <Fragment>
         <div className="d-flex ">



            {/* first card  */}
            <div className="card mr-3"  >
               <div className="card-body">
                  <h5 className="card-title text-white">Total Billing</h5>

                  <p className="card-text d-flex justify-content-around">

                     <div className="leftSide">

                        <CircularProgressbarWithChildren className='proggressBar' value={percentage}>

                           <img style={{ width: 35, marginTop: -19 }} src={BillingIcon} alt="Billing" />
                           <div style={{ fontSize: 12, marginTop: -5 }}>

                           </div>
                        </CircularProgressbarWithChildren>
                     </div>

                     <div className="RightSide">
                        <p style={{ fontSize: '38px!important' }} className="FourthCardNumber mb-0" > <span className='rsIcon'>&#8377;</span>200</p>

                        <p className="m-0 p-0 float-right"><span className='greanText' >12%</span> Growth</p>
                     </div>

                  </p>
               </div>
            </div>



            {/* second card  */}
            <div className="card mr-3"  >
               <div className="card-body">
                  <h5 className="card-title text-white">Total Patient</h5>

                  <p className="card-text d-flex justify-content-between">
                     <div className="leftSide">
                        <CircularProgressbarWithChildren className='proggressBar pateintsPer' value={Patientspercentage}>

                           <img style={{ width: 30, marginTop: -15 }} src={TotalPaitent} alt="Billing" />
                           <div style={{ fontSize: 12, marginTop: -5 }}>

                           </div>
                        </CircularProgressbarWithChildren>
                     </div>
                     <div className="RightSide">
                        <p className="FourthCardNumber" >200</p>
                        <br />
                        <p className="m-0 p-0 float-right"><span className='greanText' >10%</span> Growth</p>
                     </div>
                  </p>
               </div>
            </div>

            {/* third card  */}
            <div className="card mr-3"  >
               <div className="card-body">
                  <h5 className="card-title text-white">Today's Appointment
                  </h5>

                  <p className="card-text row">
                     <div className="leftSide col-5 pr-0">
                        <CircularProgressbarWithChildren className='proggressBar' value={Appointmentpercentage}>

                           <img style={{ width: 30, marginTop: -3, }} src={CalanderDarkIcon} alt="Billing" />
                           <div style={{ fontSize: 12, marginTop: -5 }}>

                           </div>
                        </CircularProgressbarWithChildren>
                     </div>
                     <div className="RightSide col-7 pl-0">
                        <p className="FourthCardNumber" >10</p>
                        <br />

                        <p className="CardRightText">Appointments</p>
                     </div>
                  </p>
               </div>
            </div>

            {/* Fourth card */}
            <div className="card mr-3"  >
               <div className="card-body">
                  <h5 className="card-title text-white">Pending Task</h5>

                  <p className="card-text row justify-content-between">
                     <div className="leftSide col-5 pr-0">
                        <CircularProgressbarWithChildren className='proggressBar PendingTask' value={PendingTaskpercentage}>

                           <img style={{ width: 30, marginTop: 5 }} src={PendingTask} alt="Billing" />
                           <div style={{ fontSize: 12, marginTop: -5 }}>

                           </div>
                        </CircularProgressbarWithChildren>

                     </div>
                     <div className="RightSide col-7 pl-0">
                        <p className="FourthCardNumber" >6</p>
                        <br />

                        <p className="CardRightText">till 26 Apr, 2021</p>
                     </div>

                  </p>
               </div>
            </div>


            <div className="card waitingcard mr-3" onClick={() => setLgShow(true)}  >
               <div className="card-body">
                  <h5 className="card-title  text-white">Waiting Patient</h5>

                  <p className="card-text d-flex justify-content-between">
                     <div className="leftSide">
                        <img className="mt-3 ml-1" src={Waiting} alt="" />
                     </div>
                     <div className="RightSide">
                        <p className=' mb-1 mt-2 mr-1 fifhtCardNumber'   >20</p>

                     </div>
                  </p>
               </div>
            </div>
            {
               setLgShow ? <WaitingPatiens setLgShow={setLgShow} lgShow={lgShow} /> : ''
            }



         </div>



      </Fragment >
   )
}

export default Card
