import React, { Fragment, useState } from 'react'
import './TodoList.css'
import Note from '../icons/Note.svg'
import { Pagination } from 'react-bootstrap'
 
import * as Icon from "react-icons/fi";
import Checkbox from "react-custom-checkbox";

const TodoList = () => {
   const [LineSlicer, setLineSlicer] = useState(true)
   const [LineSlicerTwo, setLineSlicerTwo] = useState(false)
   const [LineSlicerThree, setLineSlicerThree] = useState(false)
  
   return (
      <Fragment>
       
        
            <div className="card appointments col-12" >
            <div className="card-body">
               <h5 className="card-title row pt-3 pb-3">
                  <div className="col-lg-4 TodoHeader">
                     <img src={Note} alt="" className="mr-2" />
                     <span className="appointmentText "> Todo List</span>
                  </div>
                 
                  <div className="col-lg-4 mt-2 m-0 p-0">
                     <form action="">
                        <div style={{ paddingBottom: 9 }} className="SearchStyle">
                           
                           <input  placeholder="Search" type="text" className='appointmentSearchInpu' />
                           <button type='submit' className=" searchIcon ">
                              <i className="material-icons">search</i>
                              </button>
                        </div>
                     </form>
                  </div>
                  <div onClick={() => alert("Target Fired")} className="col-lg-2 mt-2 ml-5 newAppointment d-flex justify-content-between">
                    <i className="material-icons p-1 AddIcon text-white">add</i>
                     <span className="text-white addText">
                        Add 
                        
                    </span>
                    
                  </div>

               </h5>

               <p className="card-text">
                  <div className="table-responsive">
                     <table className="table table-shopping mb-1">
                        <thead>
                           <tr>
                              <th className=" text-left">
                                 Todo list item
                              </th>
                              <th className=" text-left">Due Date</th>
                              <th className=" text-right pr-2">Priority</th>
                           </tr>
                        </thead>
                        <tbody>


                           <tr>
                              <td className=" text-left d-flex">
                         
                                 
                                 
                                 
                                 
                                 <Checkbox
                                    icon={<Icon.FiCheck color="#C06C84" size={14} />}
                                    name="my-input"
                                    checked={true}
                                    className='CheckBoxInput'
                                    borderColor="#C06C84"
                                    style={{ cursor: "pointer", }}
                                   
                                    
                                 />
                                 <span className={`ml-2 paitentName ${LineSlicer ? 'lineSlicer' : ''}`}>Attend Conference</span>
                              </td>
                              
                              <td className="TodoData text-left">
                                 <span className={` ${LineSlicer ? 'lineSlicer' : ''}`}>11.11.2021</span>
                              </td >
                              <td className="TodoData text-right"><span className={` ${LineSlicer ? 'lineSlicer' : ''}`}>High</span></td>
                           </tr>
                           
                           
                           
                           <tr>
                              <td className=" text-left d-flex">
                                 <Checkbox
                                    icon={<Icon.FiCheck color="#C06C84" size={14} />}
                                    name="my-input"
                                    checked={false}
                                    className='CheckBoxInput'
                                    borderColor="#C06C84"
                                    style={{ cursor: "pointer", }}


                                 />
                                 <span className={`ml-2 paitentName ${LineSlicerTwo ? 'lineSlicer' : ''}`}>Seminar</span>
                              </td>
                              
                              <td className="TodoData text-left" >
                                 <span className={` ${LineSlicerTwo ? 'lineSlicer' : ''}`}>21.11.2021</span>
                              </td >
                              <td className="TodoData text-right"><span className={`text-success ${LineSlicerTwo ? 'lineSlicer' : ''}`}>Low</span></td>
                           </tr>
                     
                           <tr>
                              <td className=" text-left d-flex">
                                 <Checkbox
                                    icon={<Icon.FiCheck color="#C06C84" size={14} />}
                                    name="my-input"
                                    checked={false}
                                    className='CheckBoxInput'
                                    borderColor="#C06C84"
                                    style={{ cursor: "pointer", }}


                                 />
                                 <span className={`ml-2 paitentName ${LineSlicerThree ? 'lineSlicer' : ''}`}>Attend Conference</span>
                              </td>
                              
                              <td className="TodoData text-left">
                                 <span className={` ${LineSlicerThree ? 'lineSlicer' : ''}`}>11.11.2021</span>
                              </td >
                              <td className="TodoData text-right"><span className={` text-warning ${LineSlicerThree ? 'lineSlicer' : ''}`}>Medium</span></td>
                           </tr>

                           
                        </tbody>
                     </table>
                     <tfoot className="float-right mt-2">
                        
                        <tr>
                           <td>
                              <Pagination>
                                 <Pagination.Prev  className='previous'/>
                                 <Pagination.Item active>{1}</Pagination.Item>
                                 <Pagination.Ellipsis />


                                 <Pagination.Item >{12}</Pagination.Item>


                                 <Pagination.Item>{20}</Pagination.Item>
                                 <Pagination.Next className='next' />

                              </Pagination>
                           </td>
                        </tr>
                     </tfoot>
                  </div></p>
            </div>
         </div>
         </Fragment>
      
   )
}

export default TodoList
