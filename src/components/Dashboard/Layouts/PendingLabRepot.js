import React, { Fragment } from "react";
import "./PendingLabRepot.css";
import PendingLabRepotIcon from '../icons/PendingLabRepotIcon.svg'
import userOne from '../icons/userOne.png'
import UserTwo from '../icons/UserTwo.png'
import Eye from '../icons/Eye.svg'
import Note from '../icons/Note.svg'
import Bottle from '../icons/Bottle.svg'

import { Form, Pagination } from "react-bootstrap";


const PendingLabRepot = () => {
   return (
      <Fragment>
         <div className="card appointments col-12">
            <div className="card-body">
               <h5 className="card-title row pt-3 pb-3">
                  <div className="col-lg-8  PendingLabHeader">
                     <img src={PendingLabRepotIcon} alt="" className="mr-2" />
                     <span className="appointmentText ">Pending Lab Reports</span>
                  </div>

                  <div className="col-lg-4 m-0 p-0">
                     <form action="">
                        <div className="SearchStyle">
                           <input
                              placeholder="Search"
                              type="text"
                              className="appointmentSearchInpu"
                           />
                           <button type="submit" className="searchIcon  ">
                              <i className="material-icons">
                                 search
                              </i>
                           </button>
                        </div>
                     </form>
                  </div>

               </h5>

               <p className="card-text">
                  <div className="table-responsive">
                     <table className="table table-shopping">
                        <thead>
                           <tr>
                              <th className="text-left">
                                 Date

                              </th>
                              <th className="text-left">Patient Name</th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>



                           </tr>
                        </thead>
                        <tbody>


                           <tr>
                              <td className="TodoData text-left">
                                 11.11.2021
                              </td >
                              <td className='text-left'>
                                 <img src={UserTwo} alt="" />
                                 <span className="ml-2 paitentName">Kate Barke</span>
                              </td>
                              <td><img src={Eye} alt="" /></td>
                              <td><img src={Note} alt="" /></td>
                              <td><img src={Bottle} alt="" /></td>
                              <td>
                                 <div className="custom-control custom-switch">
                                    <input type="checkbox" className="custom-control-input" id="customSwitch1" />
                                    <label className="custom-control-label" htmlFor="customSwitch1"></label>
                                 </div>
                              </td>
                           </tr>

                           <tr>
                              <td className="TodoData text-left">
                                 13.11.2021
                              </td >
                              <td className='text-left'>
                                 <img src={userOne} alt="" />
                                 <span className="ml-2 paitentName">John Dscuja</span>
                              </td>
                              <td><img src={Eye} alt="" /></td>
                              <td><img src={Note} alt="" /></td>
                              <td><img src={Bottle} alt="" /></td>
                              <td>
                                 <div className="custom-control custom-switch">
                                    <input type="checkbox" className="custom-control-input" id="customSwitch2" />
                                    <label className="custom-control-label" htmlFor="customSwitch2"></label>
                                 </div>
                              </td>
                           </tr>


                        </tbody>
                     </table>
                     <tfoot className='float-right mt-2'>
                        <tr>
                           <td>
                              <Pagination>
                                 <Pagination.Prev className="previous" />
                                 <Pagination.Item active>{1}</Pagination.Item>
                                 <Pagination.Ellipsis />


                                 <Pagination.Item >{12}</Pagination.Item>


                                 <Pagination.Item>{20}</Pagination.Item>
                                 <Pagination.Next className='next' />

                              </Pagination>
                           </td>
                        </tr>
                     </tfoot>
                  </div>
               </p>

            </div>
         </div>
      </Fragment>
   );
};

export default PendingLabRepot;
