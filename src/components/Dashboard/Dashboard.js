import React, { Fragment } from 'react'
import HeaderTop from './Layouts/HeaderTop'
import PendingLabRepot from './Layouts/PendingLabRepot'
import LeftSidebar from './Layouts/LeftSidebar'
import Card from './Layouts/Card'
import Appointments from './Layouts/Appointments'
import TodoList from './Layouts/TodoList'
import './Dashboard.css'
import ProgressBarCard from './Layouts/ProgressBarCard'
import { Form } from 'react-bootstrap'


const Dashboard = () => {
   return (
      <Fragment>

         <div className="wrapper">
            {/* Left Side Bar  */}
            <LeftSidebar />
            <div className="main-panel">

               {/* Header top Section  */}
               <HeaderTop />

               {/* Main Content start Here  */}
               <div className="content pb-0">
                  <div className="container-fluid pl-0 pr-0">
                     {/* Top card SectionHere  */}

                     <Card />
                     {/* Appointments Start HEre  */}
                     <Appointments />
                     <div className="row TodoAreaMargin">

                        {/* Todo list gose there  */}
                        <div className="col-lg-6 col-md-6 col-sm-12">
                           <TodoList></TodoList>
                        </div>
                        {/* Todo list gose there  */}
                        <div className="col-lg-6 col-md-6 col-sm-12">
                           <PendingLabRepot />
                        </div>


                     </div>
                  </div>
               </div>
            </div>
         </div>
      </Fragment>
   )
}

export default Dashboard
