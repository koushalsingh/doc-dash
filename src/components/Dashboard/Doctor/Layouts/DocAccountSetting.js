import React, { Fragment } from 'react'
import styled from 'styled-components'
import ReferMan from '../../icons/ReferMan.svg'
import FollowUpButton from '../../icons/FollowUpButton.svg'
const DocAccountSetting = () => {
   return (
      <Fragment>
         <ProfileSetting>
            <div className="account">
               <form action="">
                  <div className="topSection">

                     <input placeholder='User Name ' type="text" className='form-control userName' />

                     <div className="row">
                        <div className="col-6">
                           <input className='Passowrd form-control' placeholder='Current Password' type="password" />
                        </div>
                        <div className="col-6">
                           <input className='Passowrd form-control' placeholder='New Password' type="password" />
                        </div>
                     </div>
                     <div className="text-right mr-1 mt-3 ButtomBottom">

                        <button type='submit' className="col-lg-6 mb-5 col-md-6 col-sm-12 col-xs-12 BottomButtonTwo">
                           <img className="mr-1" src={FollowUpButton} alt="" />
                        Follow Up
                      </button>
                     </div>
                  </div>

               </form>
               <div className="bottomSection">

                  <form action="">
                     <div className="row">
                        <div className="col-6">
                           <input className='form-control' placeholder='Name' type="text" />

                        </div>
                        <div className="col-6">
                           <input className='form-control' placeholder='City' type="text" />

                        </div>
                     </div>
                     <div className="row">
                        <div className="col-6">
                           <input className='form-control' placeholder='Email' type="text" />

                        </div>
                        <div className="col-6">
                           <input className='form-control' placeholder='Country' type="text" />

                        </div>
                     </div>
                     <textarea placeholder='Address' name="message" id="" className='form-control'></textarea>


                     <div className="text-right mr-1 mt-3 ButtomBottom">

                        <button type='submit' className="col-lg-6 col-md-6 col-sm-12 col-xs-12 BottomButtonTwo">
                           <img className="mr-1" src={FollowUpButton} alt="" />
                        Follow Up
                      </button>
                     </div>
                  </form>

               </div>
            </div>

         </ProfileSetting>
      </Fragment>
   )
}
const ProfileSetting = styled.div`
.ButtomBottom{
   border-top:unset!important;
}
textarea{
   height: 80px!important;
   background-image:unset!important;
}
input{
       background-image: unset!important;
    margin-top: 10px;
    margin-bottom: 20px;
}
`
export default DocAccountSetting
