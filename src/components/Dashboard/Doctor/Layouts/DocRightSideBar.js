import React, { Fragment } from 'react'
import { Tab, Tabs } from 'react-bootstrap'
import styled from 'styled-components'
import DocAccountSetting from './DocAccountSetting'

const DocRightSideBar = () => {
   return (
      <Fragment>

         <div className="card mt-0">
            <div className="card-body">
               <p className="card-text ">
                  <Tabs
                     defaultActiveKey="home"
                     transition={false}
                     id="noanim-tab-example">
                     <Tab eventKey="home" title="Medical Record">
                        <p className="text-left">
                           <DocDetails>
                              <p className='docBio'>

                                 Dr. Akul Singh is an Indian bariatric surgeon known for his work in laparoscopic and robot - assisted surgery. He is an Indian bariatric surgeon known for his work in laparoscopic and
                                 robot-assisted surgery.
                           </p>
                              <h5 className='qualificationHeader'>Qualification</h5>
                              <hr className='BOrderBotton' />
                              <div className="qua">
                                 <p className='qualification'>  <strong> Hospital Affiliations:</strong> UCSF MEDICAL CENTER</p>
                                 <p className='qualification'> <strong> Medical School:</strong> Palmer College of Chiropractic 1978</p>
                                 <p className='qualification'> <strong>Residency:</strong>  New york</p>
                                 <p className='qualification'> <strong>Certifications:</strong>  Certified Chiropractic Sports Physician 1982</p>
                                 <p className='qualification'> <strong> Gender:</strong> Female</p>
                                 <p className='qualification'> <strong> Experience / Tranining:</strong> Past-President, Int. Fed. 1991</p>
                                 <p className='qualification'> <strong> Internship:</strong> Palmer Clinic</p>
                              </div>

                              <h5 className='ServeryHeader'>Surgery</h5>
                              <hr className='BOrderBotton' />
                              <div className="servery ">
                                 <p className='row'>
                                    <input id='Breast' name='servery' className='col-1' type="radio" /><label htmlFor="Breast">Breast Surgery</label>
                                 </p>
                                 <p className='row'>
                                    <input id='Colorectal' name='servery' className='col-1' type="radio" /> <label htmlFor="Colorectal"> Colorectal Surgery</label>
                                 </p>
                                 <p className='row mb-4'>
                                    <input id='Endocrinology' name='servery' className='col-1' type="radio" /> <label htmlFor="Endocrinology">Endocrinology</label>
                                 </p>
                              </div>
                           </DocDetails>
                        </p>
                     </Tab>

                     <Tab eventKey="Notes" title="Account">
                        <p className="text-left ">
                           <ProfileUpdate>
                              <DocAccountSetting />
                           </ProfileUpdate>
                        </p>
                     </Tab>

                  </Tabs>
               </p>
            </div>
         </div>
      </Fragment>
   )
}
/* account part  */
const ProfileUpdate = styled.div`
.account{
   padding-top:20px;
   padding-left:20px;
   padding-right:20px;
}
`
const DocDetails = styled.div`


.docBio{
   
    font-family: 'Poppins';
    font-size: 14px;
    color: #646464;
    padding-top: 20px;
    padding-left: 20px;

}
hr.BOrderBotton {
    width: 94%;
}
.qualificationHeader{
   color:#1D1D1D;
   font-weight:500;
   font-size:16px;
   
    padding-top: 15px;
    padding-left: 20px;

}
.qua{
   padding-top: 5px;
    padding-left: 20px; 
    .qualification{
       font-size:15px;
       color:#4D4D4D;
       font-weight:400;
       strong{
          font-weight:500!important;
       }
    }
   
}
.ServeryHeader{
     color:#1D1D1D;
   font-weight:500;
   font-size:16px;
   
    padding-top: 15px;
    padding-left: 20px;
}
.servery{
    padding-top: 5px;
    padding-left: 20px;
    p{
        font-size:15px;
       color:#4D4D4D;
       font-weight:400;
      margin-bottom:7px;
      label{
           font-size:15px!important;
       color:#4D4D4D!important;
       font-weight:400!important;
      margin-bottom:7px!important;
      }
      }
}
`

export default DocRightSideBar
