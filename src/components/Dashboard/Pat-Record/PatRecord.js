import React from "react";
import { Card } from "react-bootstrap";

import HeaderTop from "../Layouts/HeaderTop";
import LeftSidebar from "../Layouts/LeftSidebar";
import PatImage from "../icons/PatImage.png";
import Heard from "../icons/Heard.svg";
import ReferMan from "../icons/ReferMan.svg";
import FollowUpButton from "../icons/FollowUpButton.svg";
import "./PatRecord.css";
import { Link } from "react-router-dom";
import RightSidebar from "./layouts/RightSidebar";
const PatRecord = () => {
   return (
      <>
         <div className="wrapper">
            {/* Left Side Bar  */}
            <LeftSidebar />
            <div className="main-panel">
               {/* Header top Section  */}
               <HeaderTop searchBar={true} TimeBar={true} />

               {/* Main Content start Here  */}

               <div className="content">
                  <div className="container-fluid row mt-4 mr-0 pr-0 ml-2">
                     {/* Patients record Left Side bar  */}
                     <div className="col-lg-3">
                        <Card className="mt-0">
                           <Card.Img variant="top" src={PatImage} />
                           <Card.Body>
                              {/* Card text top  */}
                              <Card.Text className="BorderBottom">
                                 <div className="PatiendInfo">
                                    <p className="m-0 p-0 text-left">Patient Name</p>
                                    <h5 className="m-0 p-0 text-left">Mr. John Doe</h5>
                                 </div>

                                 {/* Address  */}
                                 <div className="PatiendInfo">
                                    <p className="m-0 p-0 text-left">Contact Details</p>
                                    <h5 className="m-0 p-0 text-left">
                                       99/3, New York, UK
                        </h5>
                                 </div>

                                 {/* age  */}
                                 <div className="PatiendInfo">
                                    <p className="m-0 p-0 text-left">Age</p>
                                    <h5 className="m-0 p-0 text-left">45</h5>
                                 </div>
                                 {/* Blood Group  */}
                                 <div className="PatiendInfo mb-3">
                                    <p className="m-0 p-0 text-left">Blood Group</p>
                                    <h5 className="m-0 p-0 text-left">B+VE</h5>
                                 </div>
                              </Card.Text>

                              {/* card text bottom  */}
                              <div className="d-flex VS">
                                 <img src={Heard} alt="" />
                                 <h5>Vital Signs</h5>
                              </div>
                              <Card.Text className="row ">
                                 {/* Body Temp.  */}
                                 <div className="col-lg-6 pr-0">
                                    <div className="PatiendInfo">
                                       <p className="m-0 p-0 text-left">Body Temp.</p>
                                       <h5 className="m-0 p-0 text-left">94</h5>
                                    </div>

                                    {/* Pulse Rate  */}
                                    <div className="PatiendInfo">
                                       <p className="m-0 p-0 text-left">Pulse Rate</p>
                                       <h5 className="m-0 p-0 text-left">80</h5>
                                    </div>

                                    {/* Height  */}
                                    <div className="PatiendInfo">
                                       <p className="m-0 p-0 text-left">Height</p>
                                       <h5 className="m-0 p-0 text-left">5.9''</h5>
                                    </div>
                                 </div>

                                 {/* right  */}
                                 <div className="col-lg-6">
                                    <div className="VitalSigns">
                                       <p className="m-0 p-0 text-left">Blood Pres</p>
                                       <h5 className="m-0 p-0 text-left">120</h5>
                                    </div>

                                    {/* Address  */}
                                    <div className="VitalSigns">
                                       <p className="m-0 p-0 text-left">Resp. Rate</p>
                                       <h5 className="m-0 p-0 text-left">300</h5>
                                    </div>

                                    {/* weight  */}
                                    <div className="VitalSigns">
                                       <p className="m-0 p-0 text-left">Weight</p>
                                       <h5 className="m-0 p-0 text-left">65 kg</h5>
                                    </div>
                                 </div>
                                 <Link to="/" className="PreVitalSigns text-left">
                                    Previous Vital sign
                      </Link>
                              </Card.Text>

                              <div className="d-flex ButtomBottom">
                                 <button className=" col-lg-6 col-md-6 col-sm-12 col-xs-12 BottomButtonOne">
                                    <img className="mr-1" src={ReferMan} alt="" />
                        Referred to
                      </button>
                                 <button className="col-lg-6 col-md-6 col-sm-12 col-xs-12 BottomButtonTwo">
                                    <img className="mr-1" src={FollowUpButton} alt="" />
                        Follow Up
                      </button>
                              </div>
                           </Card.Body>
                        </Card>
                     </div>




                     {/* Patients record Right Side Bar  */}
                     <div className="col-lg-9">
                        <RightSidebar />

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </>
   );
};

export default PatRecord;
