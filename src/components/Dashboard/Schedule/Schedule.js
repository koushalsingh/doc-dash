import React, { Fragment, useState } from 'react'
import styled from 'styled-components'
import LeftSidebar from '../Layouts/LeftSidebar'
import HeaderTop from '../Layouts/HeaderTop'
import ScheduleTimingIcon from '../icons/ScheduleTimingIcon.svg'

import DownArrow from '../icons/DownArrow.svg'
import ClockForSchdule from '../icons/ClockForSchdule.svg'
import Appointment from '../icons/Appointment.svg'
import ClockForTimeSlot from '../icons/ClockForTimeSlot.svg'
import RefeshIcon from '../icons/RefeshIcon.svg'
import ReferMan from '../icons/ReferMan.svg'
import ScheduleTimeWhite from '../icons/ScheduleTimeWhite.svg'
import CustomRecurring from './layouts/CustomRecurring'

const Schedule = () => {
   const [lgShow, setLgShow] = useState(false);
   return (
      <Fragment>
         <div className="wrapper">
            {/* Left Side Bar  */}
            <LeftSidebar />
            <div className="main-panel">
               {/* Header top Section  */}
               <HeaderTop />

               {/* Main Content start Here  */}

               <div className="content">
                  <div className="container-fluid row">
                     {/* Patients record Left Side bar  */}
                     <div className="col-lg-12">

                        <SheduleContent>
                           {
                              lgShow ?
                                 <CustomRecurring lgShow={lgShow} setLgShow={setLgShow}></CustomRecurring>
                                 :
                                 ''
                           }
                           <div className="card appointments col-12" >
                              <div className="card-body">
                                 <h5 className="card-title row pt-3 pb-3">
                                    <div className="ml-4 TodoHeader">
                                       <img src={ScheduleTimingIcon} alt="" className="mr-1" />
                                       <span className="appointmentText "> Todo List</span>
                                    </div>
                                 </h5>
                                 <p className="card-text">
                                    <form action="">
                                       <div className="col-lg-5 pl-0 ArrowClass text-left">
                                          <label className="" htmlFor="TimeSlot">
                                             <img src={ClockForSchdule} alt="" />
                                             Time slot duration</label>
                                          <span style={{ position: 'absolute', right: '27px', top: '47px' }} className="downArrow">
                                             <img src={DownArrow} alt="" />
                                          </span>
                                          <select className='form-control inputStyle' name="SelectClinic" id="">
                                             <option value="">Select Clinic    </option>
                                             <option value="">Select Clinic</option>
                                             <option value="">Select Clinic</option>
                                             <option value="">Select Clinic</option>
                                          </select>


                                       </div>

                                       {/* Select day */}
                                       <div className=" ArrowClass text-left mt-4 ">
                                          <label className="" htmlFor="TimeSlot">
                                             <img src={Appointment} className='mr-2' alt="" />
                                             Select Day</label>
                                          <div className="row">


                                             <div id='TimeSlot' className="daySlot col-lg-10  mt-2 mb-3">
                                                <span className='DaySlotSpan' >Sunday</span>
                                                <span className='DaySlotSpan' >Monday</span>
                                                <span className='DaySlotSpan active' >Tuesdy</span>
                                                <span className='DaySlotSpan' >Wednesday</span>
                                                <span className='DaySlotSpan' >Thursday</span>
                                                <span className='DaySlotSpan' >Friday</span>
                                                <span className='DaySlotSpan' >Saturday</span>
                                             </div>
                                             <div className="col-lg-2 text-right">
                                                <button className='editButton'>Edit</button>
                                             </div>
                                          </div>
                                       </div>


                                       {/* time slot */}
                                       <div className=" ArrowClass text-left mt-4 ">
                                          <label className="" htmlFor="TimeSlot">
                                             <img src={ClockForTimeSlot} className='mr-1' alt="" />
                                             Time slot </label>
                                          <div className="row">


                                             <div id='TimeSlot' className="daySlot col-lg-10  mt-2 mb-3">
                                                <span className='TimeSlotSpan' >11:00 AM - 12:00 AM 1</span>
                                                <span className='TimeSlotSpan' >11:00 AM - 12:00 AM 1</span>
                                                <span className='TimeSlotSpan active' >11:00 AM - 12:00 AM 1</span>
                                                <span className='TimeSlotSpan' >11:00 AM - 12:00 AM 1</span>
                                                <span className='TimeSlotSpan' >11:00 AM - 12:00 AM 1</span>
                                                <span className='TimeSlotSpan' >11:00 AM - 12:00 AM 1</span>

                                             </div>
                                             <div className="col-lg-2 text-right">
                                                <button className='editButton'>Edit</button>
                                             </div>
                                          </div>
                                       </div>


                                       {/* Recurring time slot */}
                                       <div className=" ArrowClass text-left mt-4 ">
                                          <label className="" htmlFor="TimeSlot">
                                             <img src={RefeshIcon} className='mr-1' alt="" />
                                             Recurring time </label>
                                          <div className="row">
                                             <div id='TimeSlot' className="daySlot col-lg-6  mt-2 mb-3">
                                                <div className="row">
                                                   <div className="col-lg-1">
                                                      <input type="checkbox" className='form-control' />
                                                   </div>
                                                   <div className="col-lg-8 pl-0 ArrowClass text-left ">

                                                      <span style={{ position: 'absolute', right: '27px', top: '10px' }} className="downArrow">
                                                         <img src={DownArrow} alt="" />
                                                      </span>
                                                      <select className='form-control inputStyle' name="SelectClinic" id="">
                                                         <option value="">No repeat</option>
                                                         <option value="">Daily</option>
                                                         <option value="">Weekly on Monday</option>
                                                         <option value="">Weekday(Monday to Friday)</option>
                                                         <option value="">Weekend(Saturday/Sunday)</option>
                                                         <option onClick={() => alert("This is for test")} value="">Custom</option>
                                                      </select>


                                                      <select multiple className='form-control inputStyle' name="SelectClinic" id="">
                                                         <option value="">Daily</option>
                                                         <option value="">Weekly on Monday</option>
                                                         <option value="">Weekday(Monday to Friday)</option>
                                                         <option value="">Weekend(Saturday/Sunday)</option>
                                                         <option onClick={() => setLgShow(preMode => !preMode)} value="">Custom</option>
                                                      </select>


                                                   </div>
                                                </div>

                                             </div>
                                             <div className="col-lg-6 mt-5 mb-5 text-right">
                                                <button type="submit" className=" col-lg-5 BottomButtonTwo ButtonMargin">
                                                   <img className="mr-2" src={ScheduleTimeWhite} alt="" />
                                                          Schedule
                                                    </button>

                                             </div>
                                          </div>
                                       </div>





                                    </form>
                                 </p>
                              </div>
                           </div>



                        </SheduleContent>
                     </div>


                  </div>
               </div>
            </div>
         </div>

      </Fragment >
   )
}
const SheduleContent = styled.div`
.ButtonMargin{
       margin-top: 50px!important;
    margin-bottom: 50px!important;
}
select.form-control[multiple], select.form-control[size] {
    height: 145px;
}
/* TimeSlot  */
.TimeSlotSpan{
    border: 1px solid #C06C84;
    color:#C06C84;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 10px;
    padding-right: 10px;
    border-radius: 33px;
    margin-right: 8px;
    font-family:'Poppins'!important;
    font-size:10px!important;
    font-weight:400;
    transition-duration:400ms;
     :hover{
       background:#C06C84;
       color:#fff;
       transition-duration:400ms;
       border: 1px solid #C06C84;
    }
}

/* Edit Button  */
.editButton{
    color: #888888;
    background: no-repeat;
    margin-top: 9px;
    font-size: 15px;
    font-family: 'Poppins';
    font-weight: 400;
}
/* Slot Active class  */
.active{
     background:#C06C84;
       color:#fff!important;
    
       border: 1px solid #C06C84;
}
.DaySlotSpan{
    border: 1px solid #C06C84;
    color:#C06C84;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 25px;
    padding-right: 25px;
    border-radius: 33px;
    margin-right: 10px;
    font-family:'Poppins'!important;
    font-size:11px!important;
    font-weight:400;
    transition-duration:400ms;
    :hover{
       background:#C06C84;
       color:#fff;
       transition-duration:400ms;
       border: 1px solid #C06C84;
    }
}
.ArrowClass{
   position: relative;
}

`
export default Schedule
