import React, { Fragment } from "react";
import "./Appointments.css";
import Appointment from '../icons/Appointment.svg'
import Filter from '../icons/Filter.svg'
import userOne from '../icons/userOne.png'
import UserTwo from '../icons/UserTwo.png'
import UserThree from '../icons/UserThree.png'
import Note from '../icons/Note.svg'
import Clock from '../icons/Clock.svg'
import { Pagination } from "react-bootstrap";
const Appointments = () => {
   return (
      <Fragment>
         <div className="card appointments mt-0 col-lg-12" >
            <div className="card-body">
               <h5 className="card-title row  mb-0">
                  <div className="col-lg-3 mt-2 pl-0 text-left ml-4">
                     <img src={Appointment} alt="" className="mr-2" />
                     <span className="appointmentText "> Appointments</span>
                  </div>
                  <div className="col-lg-2"></div>
                  <div className="col-lg-3 mt-1 m-0 p-0">
                     <form action="">
                        <div className="SearchStyle">
                           <input placeholder="Search" type="text" className='appointmentSearchInpu' />
                           <button type='submit' className="searchIcon ">
                              <i style={{ marginTop:-2 }} className='material-icons'>search</i>
                           </button>
                        </div>
                     </form>
                  </div>
                  <div onClick={() =>alert("button Has been Clicked")} className="col-lg-3 newAppointment mr-2 ml-5 d-flex justify-content-between">
                    <i className="material-icons p-1 AddIcon text-white">add</i>
                     <span className="text-white addText">
                        Add New Appointment
                        
                    </span>
                    
                  </div>

               </h5>

               <p className="card-text">
                  <div className="table-responsive">
                     <table className="table table-shopping mb-1">
                        <thead>
                           <tr>
                              <th className="">
                                 Patient Name
                                 <img className="ml-2" onClick={() => alert("Maybe It should be filter Button")} src={Filter} alt=""/>    
                              </th>
                              <th>Last Visit</th>
                              <th className="">Next Visit</th>
                              
                              <th className="">
                                 Appt. Time
                                   <img className="ml-2" onClick={() => alert("Maybe It should be filter Button")} src={Filter} alt="" />  
                              </th>
                              
                              <th className="">
                                 Disease
                                 <img className="ml-2" onClick={() => alert("Maybe It should be filter Button")} src={Filter} alt="" />  
                              </th>
                              <th className="">
                                 Gender
                                  <img className="ml-2" onClick={() => alert("Maybe It should be filter Button")} src={Filter} alt="" />  
                              </th>
                              <th className="">
                                 Consl. Type
                                   <img className="ml-2" onClick={() => alert("Maybe It should be filter Button")} src={Filter} alt="" />  
                              </th>
                              <th>
                                 Room No.
                                <img className="ml-2" onClick={() => alert("Maybe It should be filter Button")} src={Filter} alt="" /> 
                              </th>
                              <th></th>
                              <th></th>
                            
                           </tr>
                        </thead>
                        <tbody>
                           
                           
                           <tr>
                              <td>
                                 <img src={userOne} alt="" />
                                 <span  className="ml-2 paitentName">Kate Barke</span>
                              </td>
                              <td>
                                 Nov. 11, 2021
                              </td>
                              <td>
                                 Nov. 14, 2021
                              </td>
                              <td>12:40 PM</td>
                              <td>Normal checkup</td>
                              <td>M</td>
                              <td>Fever</td>
                              <td>202</td>
                              <td>
                                 <img src={Note} alt=""/>
                              </td>
                              <td>
                                 <img src={Clock} alt=""/>
                              </td>
                           </tr>
                           
                           
                            <tr>
                              <td>
                                 <img src={UserTwo} alt="" />
                                 <span  className="ml-2 paitentName">Kate Barke</span>
                              </td>
                              <td>
                                 Nov. 11, 2021
                              </td>
                              <td>
                                 Nov. 14, 2021
                              </td>
                              <td>12:40 PM</td>
                              <td>Normal checkup</td>
                              <td>M</td>
                              <td>Fever</td>
                              <td>205</td>
                              <td>
                                 <img src={Note} alt=""/>
                              </td>
                              <td>
                                 <img src={Clock} alt=""/>
                              </td>
                           </tr>
                           
                           
                            <tr>
                              <td>
                                 <img src={UserThree} alt="" />
                                 <span  className="ml-2 paitentName">Kate Barke</span>
                              </td>
                              <td>
                                 Nov. 11, 2021
                              </td>
                              <td>
                                 Nov. 14, 2021
                              </td>
                              <td>12:40 PM</td>
                              <td>Normal checkup</td>
                              <td>F</td>
                              <td>Cold</td>
                              <td>204</td>
                              <td>
                                 <img src={Note} alt=""/>
                              </td>
                              <td>
                                 <img src={Clock} alt=""/>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                     <tfoot className='float-right'>
                           <tr >
                              <td >
                              
                                       <Pagination>
                                         
                                 <Pagination.Prev className="previous" />
                                 <Pagination.Item active>{1}</Pagination.Item>
                                          <Pagination.Ellipsis />

                                         
                                        

                                      
                                          <Pagination.Item>{20}</Pagination.Item>
                                          <Pagination.Next className='next' />
                                       
                                       </Pagination>
                                 
                              </td>
                           </tr>
                        </tfoot>
                  </div>
                  
                  
               </p>
            </div>
         </div>
      </Fragment>
   );
};

export default Appointments;
