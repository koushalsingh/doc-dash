import React, { Fragment } from 'react'
import { Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import HeaderTop from '../Layouts/HeaderTop'
import LeftSidebar from '../Layouts/LeftSidebar'
import DoctorImage from '../icons/Doctor.png'
import Heard from '../icons/Heard.svg'
import FollowUpButton from '../icons/FollowUpButton.svg'
import ReferMan from '../icons/ReferMan.svg'
import FacebookIcon from '../icons/FacebookIcon.svg'
import YoutubeIcon from '../icons/YoutubeIcon.svg'
import Instagram from '../icons/Instagram.svg'
import './Profile.css'
import styled from 'styled-components'
import DocRightSideBar from './Layouts/DocRightSideBar'
const Profile = () => {
   return (
      <Fragment>
         <div className="wrapper">
            {/* Left Side Bar  */}
            <LeftSidebar />
            <div className="main-panel">
               {/* Header top Section  */}
               <HeaderTop searchBar={true} TimeBar={true} />

               {/* Main Content start Here  */}

               <div className="content">
                  <div className="container-fluid row mt-4 pr-0 mr-0">
                     {/* Doctor record Left Side bar  */}
                     <div className="col-lg-4">
                        <Card className="mt-0">
                           <img className='DoctorImage' variant="top" src={DoctorImage} alt='' />
                           <Card.Body>
                              {/* Card text top  */}
                              <Card.Text className="BorderBottom">
                                 <div className="">
                                    <h5 className="m-0 p-0 text-cente DocName">Dr. Akul Singh</h5>
                                    <p className=" p-0 text-center DocFor">Neurologist</p>
                                 </div>
                                 <div className="socialIcon d-flex justify-content-around mr-5 mt-5 ml-5 mb-2">
                                    <a href="#">
                                       <img className='mr-2' src={FacebookIcon} alt="" />
                                    </a>
                                    <a href="#">
                                       <img src={YoutubeIcon} alt="" />
                                    </a>
                                    <a href="#">
                                       <img className='mr-2' src={Instagram} alt="" />

                                    </a>
                                 </div>


                                 <div className=" d-flex justify-content-around mr-3 mt-4 ml-3 mb-3">
                                    <div className="">
                                       <h5 className='DoctorExp'>15</h5>
                                       <p className='DocExpText'>Exp</p>
                                    </div>

                                    <div className="">
                                       <h5 className='DoctorExp'>125</h5>
                                       <p className='DocExpText'>Award</p>
                                    </div>

                                    <div className="">
                                       <h5 className='DoctorExp'>148</h5>
                                       <p className='DocExpText'>Clients</p>
                                    </div>



                                 </div>




                              </Card.Text>

                              {/* card text bottom  */}

                              <Card.Text className="row mb-5">
                                 {/* Doctor info  */}
                                 <DoctorInfo>
                                    <div className="col-lg-12 pr-0">
                                       <div className="PatiendInfo">
                                          <p className="m-0 p-0 text-left">Location:</p>
                                          <h5 className=" p-0 text-left">765 Folsom Ave, Suit 600 <br />
                                       San Francisco, CADGE 94107</h5>
                                       </div>

                                       <div className="PatiendInfo">
                                          <p className="m-0 p-0 text-left">Email Address:</p>
                                          <h5 className="p-0 text-left">akug@gmail.com</h5>
                                       </div>

                                       <div className="PatiendInfo">
                                          <p className="m-0 p-0 text-left">Phone:</p>
                                          <h5 className=" p-0 text-left">+ 202-555-0191</h5>
                                       </div>




                                    </div>
                                 </DoctorInfo>



                              </Card.Text>



                              {/* No Button Nedded */}
                              {/* 
                              <div className="d-flex ButtomBottom">
                                 <button className=" col-lg-6 col-md-6 col-sm-12 col-xs-12 BottomButtonOne">
                                    <img className="mr-1" src={ReferMan} alt="" />
                                    Referred to
                               </button>
                                 <button className="col-lg-6 col-md-6 col-sm-12 col-xs-12 BottomButtonTwo">
                                    <img className="mr-1" src={FollowUpButton} alt="" />
                                  Follow Up
                               </button>
                              </div> */}
                           </Card.Body>
                        </Card>
                     </div>




                     {/* Patients record Right Side Bar  */}
                     <div className="col-lg-8">
                        <DocRightSideBar />

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </Fragment >
   )
}
const DoctorInfo = styled.div`
div{
   .PatiendInfo{
   border-left:unset!important;
   p{
      font-size:12px!important;
      color:#7B7B7B!important;
   }
   h5{
      color:#4D4D4D!important;
      font-size:14px!important;
      margin-top:10px;
   }
}
}

`

export default Profile
