import React, { Fragment } from 'react'
import './WaitingPatients.css'
import VideoIcon from '../icons/VideoIcon.svg'
import Correct from '../icons/Correct.svg'
import Cross from '../icons/Cross.svg'

import { Dropdown, Modal } from 'react-bootstrap'

import UserTwo from '../icons/UserTwo.png'
import WaitingPatients from '../icons/WaitingPatients.svg'

const WaitingPatiens = ({ setLgShow, lgShow }) => {
   return (
      <Fragment>

         <Modal
            size="lg"
            show={lgShow}
            onHide={() => setLgShow(false)}
            aria-labelledby="example-modal-sizes-title-lg"
         >
            <Modal.Header closeButton>
               <Modal.Title id="example-modal-sizes-title-lg" className='ml-3'>
                  <img src={WaitingPatients} alt="" /> <span className="ModalTitle">Waiting Patients</span>
               </Modal.Title>
            </Modal.Header>
            <Modal.Body>
               <div className="table-responsive">
                  <table className="table table-shopping">
                     <thead>
                        <tr>
                           <th className="">
                           </th>
                           <th></th>
                           <th></th>
                           <th></th>
                           <th></th>

                        </tr>
                     </thead>
                     <tbody>


                        <tr className=''>

                           <td className='text-left pl-2'>
                              <img src={UserTwo} alt="" />
                              <span className="ml-2 paitentName">Kate Barke</span>
                           </td>

                           <td className='text-left'><img onClick={() => alert("Function Clicked")} src={VideoIcon} alt="" /></td>
                           <td className='text-center pl-3 pr-3'><img onClick={() => alert("Function Clicked")} src={Correct} alt="" /></td>
                           <td className='text-right'><img onClick={() => alert("Function Clicked")} src={Cross} alt="" /></td>

                           <td className='text-right'>
                              <Dropdown>
                                 <Dropdown.Toggle variant="success" id="dropdown-basic" className="PickASlot">
                                    Pick a Slot
                                 </Dropdown.Toggle>
                                 <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                 </Dropdown.Menu>
                              </Dropdown>

                           </td>

                        </tr>
                        <hr className='modalHr' />
                        <tr>

                           <td className='text-left pl-2'>
                              <img src={UserTwo} alt="" />
                              <span className="ml-2 paitentName">Kate Barke</span>
                           </td>

                           <td className='text-left'><img onClick={() => alert("Function Clicked")} src={VideoIcon} alt="" /></td>

                           <td className='text-center pl-3 pr-3'><img onClick={() => alert("Function Clicked")} src={Correct} alt="" /></td>

                           <td className='text-right'><img onClick={() => alert("Function Clicked")} src={Cross} alt="" /></td>

                           <td className='text-right'>
                              <Dropdown>
                                 <Dropdown.Toggle variant="success" id="dropdown-basic" className="PickASlot">
                                    Pick a Slot
                                 </Dropdown.Toggle>

                                 <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                 </Dropdown.Menu>
                              </Dropdown>

                           </td>
                        </tr>
                        <hr className='modalHr' />
                        <tr>

                           <td className='text-left pl-2'>
                              <img src={UserTwo} alt="" />
                              <span className="ml-2 paitentName">Kate Barke</span>
                           </td>

                           <td className='text-left'><img onClick={() => alert("Function Clicked")} src={VideoIcon} alt="" /></td>

                           <td className='text-center pl-3 pr-3'><img onClick={() => alert("Function Clicked")} src={Correct} alt="" /></td>

                           <td className='text-right'><img onClick={() => alert("Function Clicked")} src={Cross} alt="" /></td>

                           <td className='text-right'>
                              <Dropdown>
                                 <Dropdown.Toggle variant="success" id="dropdown-basic" className="PickASlot">
                                    Pick a Slot
                                 </Dropdown.Toggle>

                                 <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                 </Dropdown.Menu>
                              </Dropdown>

                           </td>
                        </tr>
                        <hr className='modalHr' />
                        <tr>

                           <td className='text-left pl-2'>
                              <img src={UserTwo} alt="" />
                              <span className="ml-2 paitentName">Kate Barke</span>
                           </td>

                           <td className='text-left'><img onClick={() => alert("Function Clicked")} src={VideoIcon} alt="" /></td>

                           <td className='text-center pl-3 pr-3'><img onClick={() => alert("Function Clicked")} src={Correct} alt="" /></td>

                           <td className='text-right'><img onClick={() => alert("Function Clicked")} src={Cross} alt="" /></td>

                           <td className='text-right'>
                              <Dropdown>
                                 <Dropdown.Toggle variant="success" id="dropdown-basic" className="PickASlot">
                                    Pick a Slot
                                 </Dropdown.Toggle>

                                 <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                 </Dropdown.Menu>
                              </Dropdown>

                           </td>
                        </tr>








                     </tbody>
                  </table>
               </div>

            </Modal.Body>
         </Modal>


      </Fragment>
   )
}

export default WaitingPatiens
