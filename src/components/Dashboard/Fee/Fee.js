import React, { Fragment } from 'react'
import HeaderTop from '../Layouts/HeaderTop'
import LeftSidebar from '../Layouts/LeftSidebar'
import Appointment from '../icons/Appointment.svg'
import '../Layouts/Card.css'
import styled from 'styled-components'
import RefeshIcon from '../icons/RefeshWhiteIcon.svg'
import PencilIcon from '../icons/PencilIcon.svg'
function Fee() {
   return (
      <Fragment>
         <div className="wrapper">
            {/* Left Side Bar  */}
            <LeftSidebar />
            <div className="main-panel">
               {/* Header top Section  */}
               <HeaderTop searchBar={false} TimeBar={false} />

               {/* Main Content start Here  */}

               <div className="content mr-0 pr-0">
                  <div className="container-fluid row mt-4 pr-0 mr-0">
                     {/* Doctor record Left Side bar  */}
                     <div className="col-lg-12">
                        <div className="card appointments mt-0 col-lg-12">
                           <div style={{ minHeight: '82vh' }} className="card-body">
                              <h5 className="card-title row pt-3 pb-3 text-left pl-4">
                                 <div className="col-lg-3 pl-0 ">
                                    <img src={Appointment} alt="" className="mr-1" />
                                    <span className="appointmentText"> Fee Setting</span>
                                 </div>
                              </h5>

                              <p className="card-text">
                                 <CardContent>
                                    <div className="row">
                                       {/* card One  */}
                                       <div className="col-lg-4">
                                          <div className="price-card mr-3"  >
                                             <div className="price-card-body mb-3">
                                                <h5 className="price-card-title text-white">Total Billing</h5>

                                                <p className="card-text">
                                                   <div className="card-gorup ml-2 mr-2 mt-2  d-flex justify-content-between">
                                                      <h4>F2F</h4>
                                                      <div className="price-content">

                                                         <span className='rsIcon'>&#8377;</span>
                                                         <span className='Amount'>100</span>
                                                      </div>

                                                   </div>

                                                   <div className="card-gorup ml-2 mr-2 mt-2  d-flex justify-content-between">
                                                      <h4>VC</h4>
                                                      <div className="price-content">

                                                         <span className='rsIcon'>&#8377;</span>
                                                         <span className='Amount'>20</span>
                                                      </div>

                                                   </div>


                                                </p>
                                             </div>
                                          </div>
                                       </div>

                                       {/* card Two  */}
                                       <div className="col-lg-4">
                                          <div className="price-card mr-3"  >
                                             <div className="price-card-body mb-3">
                                                <h5 className="price-card-title text-white">Late Night</h5>

                                                <p className="card-text">
                                                   <div className="card-gorup ml-2 mr-2 mt-2  d-flex justify-content-between">
                                                      <h4>F2F</h4>
                                                      <div className="price-content">

                                                         <span className='rsIcon'>&#8377;</span>
                                                         <span className='Amount'>100</span>
                                                      </div>

                                                   </div>

                                                   <div className="card-gorup ml-2 mr-2 mt-2  d-flex justify-content-between">
                                                      <h4>VC</h4>
                                                      <div className="price-content">

                                                         <span className='rsIcon'>&#8377;</span>
                                                         <span className='Amount'>200</span>
                                                      </div>

                                                   </div>


                                                </p>
                                             </div>
                                          </div>
                                       </div>

                                       {/* card Three  */}
                                       <div className="col-lg-4">
                                          <div className="price-card mr-3"  >
                                             <div className="price-card-body mb-3">
                                                <h5 className="price-card-title text-white">Weekends</h5>

                                                <p className="card-text">
                                                   <div className="card-gorup ml-2 mr-2 mt-2  d-flex justify-content-between">
                                                      <h4>F2F</h4>
                                                      <div className="price-content">

                                                         <span className='rsIcon'>&#8377;</span>
                                                         <span className='Amount'>100</span>
                                                      </div>

                                                   </div>

                                                   <div className="card-gorup ml-2 mr-2 mt-2  d-flex justify-content-between">
                                                      <h4>VC</h4>
                                                      <div className="price-content">

                                                         <span className='rsIcon'>&#8377;</span>
                                                         <span className='Amount'>200</span>
                                                      </div>

                                                   </div>


                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </CardContent>
                              </p>


                              <BottonButton className='mr-3 mt-5'>
                                 <button>
                                    <img className='mr-2' src={PencilIcon} alt="" />
                                    Edit</button>
                                 <button>
                                    <img src={RefeshIcon} className='mr-2' alt="" />
                                    Update</button>
                              </BottonButton>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>

      </Fragment>
   )
}
const BottonButton = styled.div`
text-align:right;
button{
   padding-top: 9px;
    padding-bottom: 9px;
    padding-left: 30px;
    padding-right: 30px;
    margin-right: 10px;
    border-radius: 68px;
    color: #fff;
   font-family:"Poppins";
   font-size:15px;
  box-shadow: 2px 3px 15px 0px #c06c84c7;
    background-image: linear-gradient(84deg, #C06C84, #D4859C);
}


`

const CardContent = styled.div`
.card-gorup{
   
   h4{
    font-size: 22px;
    font-family: 'Poppins';
    color: #707070;
    font-weight: 500;
   }
   .price-content{
      font-size: 22px;
    font-family: 'Poppins';
    color: #707070;
    font-weight: 500;
   }
}
.price-card{
   border:1px solid #D5D5D5;
   border-radius:10px;
   .price-card-body{
      .price-card-title{
         background:#C06C84;
         border-radius:10px 10px 0 0;
         padding-top: 8px;
         padding-bottom: 8px;
         
        
      }
   }
    :hover{
           box-shadow:transparent!important;
           .price-card-title{
              background:#C06C84!important;
           }
}

`



export default Fee
