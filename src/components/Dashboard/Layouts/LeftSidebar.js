import React, { Fragment, useState } from 'react'

import { Link, NavLink } from 'react-router-dom'
import styled from 'styled-components'
import Logo from '../../../Logo.svg'
import Doctor from '../icons/Doctor.png'
import AddIcon from '../icons/circle-plus.png'
import RemoveIcon from '../icons/circle-minus.png'

import Server from '../icons/Server.svg'
import MedicitIcon from '../icons/MedicitIcon.svg'
import RepotingIcon from '../icons/RepotingIcon.svg'
import SettingIcon from '../icons/SettingIcon.svg'


const LeftSidebar = () => {
   const [ShowDropDown, setShowDropDown] = useState(false)
   const [ShowSettingDrop, setShowSettingDrop] = useState(false)
   return (
      <Fragment>
         <LeftSidebarContent>
            <div className="sidebar" data-color="white" data-background-color="black">

               <div className="logo">

                  <img src={Logo} alt="" />
               </div>
               <div className="mt-4">
                  <img src={Doctor} alt="" />
                  <h3 className='text-center text-white mt-3 DoctorName'>Dr. Singh</h3>
               </div>
               <div className="sidebar-wrapper">
                  <ul className="nav">

                     <li className='nav-item '>
                        <NavLink to='/' exact className="nav-link navLink " >
                           <i className="material-icons">
                              dashboard

                           </i>
                           <p className="activeP">Dashboard</p>
                        </NavLink>

                     </li>


                     {/* schedule */}
                     <li className="nav-item   ">
                        <NavLink to='/schedule' className="nav-link" >
                        
                              <i className='material-icons'>
                                 date_range
                     
                           </i>
                           <p>Schedules</p>
                        </NavLink>
                     </li>

                     {/* Calendar js  */}
                     <li className="nav-item   ">
                        <NavLink to='/calander' className="nav-link" >
                           <i className='material-icons'>
                              date_range
                           </i>
                           <p>Calendar</p>
                        </NavLink>
                     </li>
                     <li className='nav-item' onClick={() => setShowDropDown(preMode => !preMode)}>
                        <a className="nav-link d-flex">
                           <i >
                              <img style={{height:20}} className="" src={Server} alt="" />
                           </i>

                           <p className='mr-2'>Patient records</p>
                           {
                              ShowDropDown ?
                                 <img onClick={() => setShowDropDown(true)} className='addIcon ml-2' src={RemoveIcon} alt="" />
                                 :
                                 <img onClick={() => setShowDropDown(false)} className='addIcon ml-2' src={AddIcon} alt="" />


                           }
                        </a>
                     </li>
                     {
                        ShowDropDown ? <>

                           <li className="nav-item dropdownLi">
                              <NavLink to='/pat-record' className='nav-link' >

                                 <p className='mr-2'>Patient Details</p>
                              </NavLink>
                           </li>

                        </> :
                           ''
                     }
                     <li className="nav-item ">
                        <a className="nav-link" href="./user.html">
                           <i >
                              <img className="addIcon" src={MedicitIcon} alt="" />
                           </i>
                           <p>Resources</p>
                        </a>
                     </li>
                     <li className="nav-item ">
                        <a className="nav-link" href="./user.html">
                           <i >
                              <img className="addIcon" src={RepotingIcon} alt="" />
                           </i>

                           <p>Reporting</p>
                        </a>
                     </li>


                     {/* setting start here  */}

                     <li className='nav-item ' onClick={() => setShowSettingDrop(preMode => !preMode)}>
                        <a className="nav-link d-flex">
                           <i >
                              <img className="addIcon" src={SettingIcon} alt="" />
                           </i>
                           <p className='mr-4'>Setting</p>
                           {
                              ShowSettingDrop ?
                                 <img style={{ cursor: 'grab' }} className='addIcon StISpace' src={RemoveIcon} alt="" />
                                 :
                                 <img style={{ cursor: 'grab' }} className='addIcon StISpace' src={AddIcon} alt="" />
                           }
                        </a>
                     </li>
                     {
                        ShowSettingDrop ? <>

                           <div className="dropdownLi">
                              <li className="nav-item ">
                                 <a className='nav-link' >
                                    <p className='mr-2'>Invoice</p>
                                 </a>
                              </li>

                              <li className="nav-item ">
                                 <NavLink to='/profile'  className='nav-link' >
                                    <p className='mr-2'>Profile</p>
                                 </NavLink>
                              </li>

                              <li className="nav-item ">
                                 <NavLink to='/fee' className='nav-link' >
                                    <p className='mr-2'>Fee</p>
                                 </NavLink>
                              </li>

                              <li className="nav-item ">
                                 <a className='nav-link' >
                                    <p className='mr-2'>Notification</p>
                                 </a>
                              </li>
                              <li className="nav-item ">
                                 <a className='nav-link' >
                                    <p className='mr-2'>Payment</p>
                                 </a>
                              </li>
                              <li className="nav-item ">
                                 <NavLink exact to='/membership' className='nav-link' >
                                    <p className='mr-2'>Membership</p>
                                 </NavLink>
                              </li>

                         </div>


                        </> :
                           ''
                     }
                     <div style={{ paddingBottom: '100px' }} className="mb-5"></div>
                  </ul>
               </div>
            </div>

         </LeftSidebarContent>
      </Fragment>
   )
}
const LeftSidebarContent = styled.div`
.nav-link{margin-top:10px!important;}
.StISpace{
   margin-left:63px;
}
/* .dropdownLi:hover{
   background: #0000004a;
   .nav-link{
    background:#fff;
    p{
       color:rgb(108,91,123)!important;
    }  
   }
} */
.dropdownLi{
   background: #0505053b;
    padding-top: 9px;
    padding-bottom: 19px;
    margin-left: 14px;
    margin-right: 18px;
    border-radius: 4px;
    transition:700ms;
}
.addIcon{
   width: 28px;
    height: 27px;
    margin-top: 2px;
  
}
.active:hover{
    background: #fff!important;
    z-index:999999;
    i{
       color:rgb(108,91,123)!important;
    }
    p{
     color:rgb(108,91,123)!important;
      font-family:"Poppins";
      font-weight:400!important;
    } 
}
.active {
    background: #fff;
    z-index:999999;
    i{
       color:rgb(108,91,123)!important;
    }
    p{
     color:rgb(108,91,123)!important;
      font-family:"Poppins";
      font-weight:400!important;
    } 
}

/* .a.nav-link.navLink.active  .activeP{
   color:rgb(108,91,123)!important;
} */
.DoctorName {
   padding-bottom:15px;
   width:85%;
   margin: 0 auto;
   border-bottom:1px solid rgb(0 0 0 / 10%)!important;

}
.sidebar .logo:after {
    content: '';
    position: absolute;
    bottom: 0;
    right: 0px!important;
    height: 1px;
    width: 100%;
    background-color: rgb(0 0 0 / 10%)!important;
}
.DoctorName{
       font-family: 'Poppins';
    font-size: 17px;
    font-weight: 500;
    letter-spacing: .7px;
}

.nav-item a{
   box-shadow:none!important;
   color:rgb(108,91,123)!important;

   text-align:left;
  padding: 0!important;
    padding-left: 15px!important;
    padding-top: 5px!important;
    padding-bottom: 5px!important;
    border-radius: 6px!important;
   font-family:"Poppins"!important;
   p{
     font-size:17px!important;
       font-weight: 300;
       color: #fff;
   };
}
   .sidebar{
      background:rgb(108,91,123);
      box-shadow:none;
      
      
      .logo a{
       color:#fff!important; 
       text-transform:unset;
      };
   };
   
   
`
export default LeftSidebar
