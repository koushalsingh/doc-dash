import React, { Fragment } from "react";
import HeaderTop from "../Layouts/HeaderTop";
import LeftSidebar from "../Layouts/LeftSidebar";
import Appointment from "../icons/Appointment.svg";
import './Index.css'
import styled from "styled-components";
import { Dropdown } from "react-bootstrap";

function Index() {
   return (
      <Fragment>
         <div className="wrapper">
            {/* Left Side Bar  */}
            <LeftSidebar />
            <div className="main-panel">
               {/* Header top Section  */}
               <HeaderTop searchBar={false} TimeBar={false} />

               {/* Main Content start Here  */}

               <div className="content mr-0 pr-0">
                  <div className="container-fluid row mt-4 pr-0 mr-0">
                     {/* Doctor record Left Side bar  */}
                     <div className="col-lg-12">
                        <div className="card appointments mt-0 col-lg-12">
                           <div className="card-body">
                              <h5 className="card-title row pt-3 pb-3">
                                 <div className="col-lg-3 pl-0 ">
                                    <img src={Appointment} alt="" className="mr-2" />
                                    <span className="appointmentText"> Membership</span>
                                 </div>
                              </h5>

                              <p className="card-text">
                                 <PriceingCard>
                                    <div className="row">
                                       {/* First Card  */}
                                       <div className="col-lg-3 mr-3">
                                          <div className="PriceCard text-left">
                                             <div className="price-group">
                                                <h4 className='text-center packegName'>Personal</h4>

                                                <div className="d-flex mb-3">
                                                   <span className="SameBorder"></span>
                                                   <span className='PurpleBorder'></span>
                                                   <span className="SameBorder"></span>
                                                </div>
                                                <label htmlFor="price ">Set Price</label>
                                                <div className="d-flex justify-content-around">
                                                   <input type="text" className='mr-1 form-control' />
                                                   <span className='mt-2 ml-1'>/</span>
                                                   <input type="text" className='ml-1 form-control' />
                                                </div>
                                             </div>

                                             <div className="price-group mt-4">
                                                <label htmlFor="price ">Paid</label>
                                                <Dropdown>
                                                   <Dropdown.Toggle variant="" id="dropdown-basic ">
                                                      Annually
                                                   </Dropdown.Toggle>

                                                   <Dropdown.Menu>
                                                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                                      <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                                      <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                                   </Dropdown.Menu>
                                                </Dropdown>
                                             </div>


                                          </div>
                                       </div>


                                       {/* Second Card  */}
                                       <div className="col-lg-3 mr-3 mr-3 ">
                                          <div className="PriceCard text-left">
                                             <div className="price-group">
                                                <div className="priceCardHeader">
                                                   <h4 className='text-center packegName'>Family</h4>

                                                   <div className="d-flex mb-3">
                                                      <span className="SameBorder"></span>
                                                      <span className='PurpleBorder'></span>
                                                      <span className="SameBorder"></span>
                                                   </div>
                                                </div>
                                                <label htmlFor="price ">Set Price</label>
                                                <div className="d-flex justify-content-around">
                                                   <input type="text" className='mr-1 form-control' />
                                                   <span className='mt-2 ml-1'>/</span>
                                                   <input type="text" className='ml-1 form-control' />
                                                </div>
                                             </div>

                                             <div className="price-group  mt-3">
                                                <label htmlFor="price ">Paid</label>
                                                <Dropdown>
                                                   <Dropdown.Toggle variant="" id="dropdown-basic ">
                                                      Annually
                                                   </Dropdown.Toggle>

                                                   <Dropdown.Menu>
                                                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                                      <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                                      <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                                   </Dropdown.Menu>
                                                </Dropdown>
                                             </div>

                                             <div className="price-group mt-2 ">
                                                <label htmlFor="price ">No. of Members</label>

                                                <div className="pl-0 col-5">
                                                   <input className=' form-control' type="number" />
                                                </div>


                                             </div>


                                          </div>
                                       </div>


                                       {/* Third Card  */}
                                       <div className="col-lg-3">
                                          <div className="PriceCard  ">
                                             <div className="price-group PriceCardAdd">
                                                <span>+</span>
                                                <h6>Add Another Plan</h6>
                                             </div>




                                          </div>
                                       </div>

                                    </div>
                                 </PriceingCard>
                              </p>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </Fragment>
   );
}
const PriceingCard = styled.div`
.packegName{
   color:#6C5B7B;
   font-family:'Poppins';
   font-size:24px;
   font-weight:500;
}
.PurpleBorder{
   background: #6C5B7B;
   height: 3px;
   width: 60px;
   margin-right: 1px;
   margin-left:1px;
}
.SameBorder{
    background: #C06C84;
    height: 3px;
    color: red;
    width: 60px;
}
input{
       background-image: unset!important;
 
}
.PriceCardAdd{
   display: flex;
    flex-direction: column;
    min-height: 40vh;
    justify-content: center;
    vertical-align: middle;
    text-align: center;
    font-size: 81px;
    font-weight: 600;
  
    border-radius: 91px;
    span{
       color:#EEEEEE;
         background:  #F9F9F9;
         border-radius:999px;
             padding-top: 30px;
    padding-bottom: 30px;
    }
    h6{
       text-transform:capitalize;
       margin-top:20px;
       color:#8B8B8B;
       font-size:16px;
    }
    
}
.dropdown{
   button{
      width:100%;
      box-shadow:none;
      border:1px solid #CECECE;
      text-transform: capitalize;

   }
}
.PriceCard{
   border:1px solid #CECECE;
padding:20px;
   margin-top:20px;
   border-radius:10px;
       min-height: 50vh;
       
       margin-bottom:110px;
}
`
export default Index;
