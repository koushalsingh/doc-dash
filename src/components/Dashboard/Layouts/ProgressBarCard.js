import React from 'react'

const ProgressBarCard = () => {
   return (
      <div>
         <div className="page-content page-container" id="page-content">
            <div className="padding">
               <div className="row container d-flex justify-content-center">
                  <div className="col-md-6 grid-margin grid-margin-md-0 stretch-card">
                     <div className="card">
                        <div className="card-body">
                           <h4 className="card-title">Circle progress bar Example </h4>
                           <div className="row px-2 template-demo">
                              <div className="col-sm-3 col-md-4 col-6 circle-progress-block">
                                 <div id="circleProgress1" className="progressbar-js-circle border rounded p-3"><svg viewBox="0 0 100 100" style="display: block; width: 100%;">
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="#eee" stroke-width="4" fill-opacity="0"></path>
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="rgb(159,162,179)" stroke-width="4" fill-opacity="0" style="stroke-dasharray: 301.635, 301.635; stroke-dashoffset: 199.079;"></path>
                                 </svg>
                                    <div className="progressbar-text" style="position: absolute; left: 50%; top: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: rgb(0, 0, 0); font-size: 1rem;">34</div>
                                 </div>
                              </div>
                              <div className="col-sm-3 col-md-4 col-6 circle-progress-block">
                                 <div id="circleProgress2" className="progressbar-js-circle border rounded p-3"><svg viewBox="0 0 100 100" style="display: block; width: 100%;">
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="#eee" stroke-width="4" fill-opacity="0"></path>
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="rgb(108,185,124)" stroke-width="4" fill-opacity="0" style="stroke-dasharray: 301.635, 301.635; stroke-dashoffset: 138.752;"></path>
                                 </svg>
                                    <div className="progressbar-text" style="position: absolute; left: 50%; top: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: rgb(0, 0, 0); font-size: 1rem;">54</div>
                                 </div>
                              </div>
                              <div className="col-sm-3 col-md-4 col-6 circle-progress-block">
                                 <div id="circleProgress3" className="progressbar-js-circle border rounded p-3"><svg viewBox="0 0 100 100" style="display: block; width: 100%;">
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="#eee" stroke-width="4" fill-opacity="0"></path>
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="rgb(198,145,145)" stroke-width="4" fill-opacity="0" style="stroke-dasharray: 301.635, 301.635; stroke-dashoffset: 165.899;"></path>
                                 </svg>
                                    <div className="progressbar-text" style="position: absolute; left: 50%; top: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: rgb(0, 0, 0); font-size: 1rem;">45</div>
                                 </div>
                              </div>
                              <div className="col-sm-3 col-md-4 col-6 circle-progress-block">
                                 <div id="circleProgress4" className="progressbar-js-circle border rounded p-3"><svg viewBox="0 0 100 100" style="display: block; width: 100%;">
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="#eee" stroke-width="4" fill-opacity="0"></path>
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="rgb(175,169,163)" stroke-width="4" fill-opacity="0" style="stroke-dasharray: 301.635, 301.635; stroke-dashoffset: 220.194;"></path>
                                 </svg>
                                    <div className="progressbar-text" style="position: absolute; left: 50%; top: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: rgb(0, 0, 0); font-size: 1rem;">27</div>
                                 </div>
                              </div>
                              <div className="col-sm-3 col-md-4 col-6 circle-progress-block">
                                 <div id="circleProgress5" className="progressbar-js-circle border rounded p-3"><svg viewBox="0 0 100 100" style="display: block; width: 100%;">
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="#eee" stroke-width="4" fill-opacity="0"></path>
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="rgb(98,194,205)" stroke-width="4" fill-opacity="0" style="stroke-dasharray: 301.635, 301.635; stroke-dashoffset: 99.5396;"></path>
                                 </svg>
                                    <div className="progressbar-text" style="position: absolute; left: 50%; top: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: rgb(0, 0, 0); font-size: 1rem;">67</div>
                                 </div>
                              </div>
                              <div className="col-sm-3 col-md-4 col-6 circle-progress-block">
                                 <div id="circleProgress6" className="progressbar-js-circle border rounded p-3"><svg viewBox="0 0 100 100" style="display: block; width: 100%;">
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="#eee" stroke-width="4" fill-opacity="0"></path>
                                    <path d="M 50,50 m 0,-48 a 48,48 0 1 1 0,96 a 48,48 0 1 1 0,-96" stroke="rgb(42,46,59)" stroke-width="4" fill-opacity="0" style="stroke-dasharray: 301.635, 301.635; stroke-dashoffset: 15.0818;"></path>
                                 </svg>
                                    <div className="progressbar-text" style="position: absolute; left: 50%; top: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: rgb(0, 0, 0); font-size: 1rem;">95</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   )
}

export default ProgressBarCard
