import React, { Fragment } from 'react'
import styled from 'styled-components'
import LeftSidebar from '../Layouts/LeftSidebar'
import HeaderTop from '../Layouts/HeaderTop'
import './Calendar.css'

import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'

const Calendar = () => {
   return (
      <Fragment>
         <div className="wrapper">
            {/* Left Side Bar  */}
            <LeftSidebar />
            <div className="main-panel">
               {/* Header top Section  */}
               <HeaderTop />

               {/* Main Content start Here  */}

               <div className="content">
                  <div className="container-fluid row mt-4">
                     {/* Patients record Left Side bar  */}
                     <div className="col-lg-12">
                        <SheduleCalendar>
                           <FullCalendar

                              plugins={[dayGridPlugin]}
                              initialView="dayGridMonth"

                              weekends={true}
                              events={[
                                 { title: 'event 1', date: '2021-04-13' },
                                 { title: 'event 2', date: '2021-04-14' }
                              ]}
                           />
                        </SheduleCalendar>
                     </div>


                  </div>
               </div>
            </div>
         </div>

      </Fragment >
   )
}
const SheduleCalendar = styled.div`
td{
  border:1px solid #DDDDDD!important
}

`
export default Calendar
