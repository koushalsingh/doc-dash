import React, { Fragment } from 'react'
import './HeaderTop.css'
import DashobardIcon from '../icons/Dashboard.svg'
import RedDot from '../icons/RedDot.svg'
import Video from '../icons/Video.svg'
import { Link } from 'react-router-dom'
import MessageIcon from '../icons/Message.svg'
import CalculatorIcon from '../icons/Calculator.svg'
import ChatIcon from '../icons/ChatIcon.svg'
import NotificationIcon from '../icons/NotificationIcon.svg'
import SignOutIcon from '../icons/SignOutIcon.svg'
const HeaderTop = ({ searchBar, TimeBar }) => {
   return (
      <Fragment>
         <nav className="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
            <div className="container-fluid">
               <div className="navbar-wrapper">
                  <Link className="navbar-brand " to="/" ><img src={DashobardIcon} alt="" /></Link>
                  {
                     searchBar ?
                        <>

                           <img className="headerContentImg" src={RedDot} alt="" />
                           <p className="headerContentPera">Appointment is next <span className="headerContentTime">00:30</span> Min</p>
                           <img className='HeaderContentVideo' src={Video} alt="" />

                        </>

                        : ''
                  }
               </div>
               <button className="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="navbar-toggler-icon icon-bar"></span>
                  <span className="navbar-toggler-icon icon-bar"></span>
                  <span className="navbar-toggler-icon icon-bar"></span>
               </button>
               <div className="collapse navbar-collapse justify-content-end">
                  {
                     TimeBar ? <form className="navbar-form">
                        <div className="input-group no-border">
                           <input type="text" value="" className="form-control" placeholder="Search..." />
                           <button type="submit" className="btn btn-white btn-round btn-just-icon">
                              <i className="material-icons">search</i>
                              <div className="ripple-container"></div>
                           </button>
                        </div>
                     </form>
                        : ''
                  }
                  <ul className="navbar-nav">
                     <li className="nav-item">
                        <a className="nav-link" href="javascript:;">
                           <img src={MessageIcon} alt="" />
                           <p className="d-lg-none d-md-block">
                              Stats
                           </p>
                        </a>
                     </li>
                     <li className="nav-item dropdown">
                        <a className="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <img src={CalculatorIcon} alt="" />
                           {/* <span className="notification">5</span> */}
                           <p className="d-lg-none d-md-block">
                              Some Actions
                        </p>
                        </a>
                        {/* <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                           <a className="dropdown-item" href="#">Mike John responded to your email</a>
                           <a className="dropdown-item" href="#">You have 5 new tasks</a>
                           <a className="dropdown-item" href="#">You're now friend with Andrew</a>
                           <a className="dropdown-item" href="#">Another Notification</a>
                           <a className="dropdown-item" href="#">Another One</a>
                        </div> */}
                     </li>


                     <li className="nav-item dropdown">
                        <a className="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <img src={ChatIcon} alt="" />
                           <p className="d-lg-none d-md-block">
                              Account
                         </p>
                        </a>
                        {/* <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                           <a className="dropdown-item" href="#">Profile</a>
                           <a className="dropdown-item" href="#">Settings</a>
                           <div className="dropdown-divider"></div>
                           <a className="dropdown-item" href="#">Log out</a>
                        </div> */}
                     </li>
                     <li className="nav-item">
                        <a className="nav-link" href="javascript:;">
                           <img src={NotificationIcon} alt="" />
                           <p className="d-lg-none d-md-block">
                              Stats
                           </p>
                        </a>
                     </li>
                     <li className="nav-item">
                        <a className="nav-link" href="javascript:;">
                           <img src={SignOutIcon} alt="" />
                           <p className="d-lg-none d-md-block">
                              LogOut
                           </p>
                        </a>
                     </li>



                  </ul>
               </div>
            </div>
         </nav>

      </Fragment>
   )
}

export default HeaderTop
