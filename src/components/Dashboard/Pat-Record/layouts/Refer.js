import React, { Fragment, } from 'react'
import { Modal } from 'react-bootstrap'
import DownArrow from '../../icons/DownArrow.svg'
import ReferMan from '../../icons/ReferManDark.svg'
import ReferManLight from '../../icons/ReferMan.svg'
import './Refer.css'

const Refer = ({ lgShowRef, setLgShowRef }) => {
   return (
      <Fragment>
         <Modal
            size="lg"
            show={lgShowRef}
            onHide={() => setLgShowRef(false)}
            aria-labelledby="example-modal-sizes-title-lg"
         >
            <Modal.Header closeButton>
               <Modal.Title id="example-modal-sizes-title-lg">
                  <span className="ModalTitle">
                     <img className='mr-2 ml-1 ReferManDark' src={ReferMan} alt="" />
                     Refered</span>
               </Modal.Title>
            </Modal.Header>
            <Modal.Body className="pb-0">
               <form action="">
                  <div className="table-responsive">
                     <table className="table table-shopping mb-0">
                        <thead>
                           <tr>
                              <th className="">
                              </th>

                           </tr>
                        </thead>
                        <tbody>
                           <tr className="d-flex justify-content-between">
                              <td className='w-100 InputGap'>
                                 <input width='40' type="text" placeholder="Select Doctor" className="form-control inputStyle " />
                              </td>
                              <td className='w-100 '>
                                 <span style={{ position: 'absolute', right: '17px', top: '15px', zIndex: '09999' }} className="downArrow">
                                    <img src={DownArrow} alt="" />
                                 </span>
                                 <select className='form-control inputStyle' name="SelectClinic" id="">
                                    <option value="">Select Clinic    </option>
                                    <option value="">Select Clinic</option>
                                    <option value="">Select Clinic</option>
                                    <option value="">Select Clinic</option>
                                 </select>

                              </td>

                           </tr>
                           <tr>
                              <td className='pb-0 pt-4'>
                                 <b>Others</b>
                              </td>
                           </tr>
                           <tr className="d-flex justify-content-between">
                              <td className='w-100 InputGap'>
                                 <input type="text" placeholder="Add Doctor Name" className="form-control inputStyle" />
                              </td>
                              <td className='w-100'>
                                 <input type="number" placeholder="Phone No." className="form-control inputStyle" />
                              </td>
                           </tr>
                           <tr className="d-flex justify-content-between mt-2">
                              <td className='w-100 InputGap'>
                                 <input type="email" placeholder="Add Email" className="form-control inputStyle" />
                              </td>
                              <td className='w-100'>
                                 <input type="email" placeholder="Add clinic name" className="form-control inputStyle" />
                              </td>
                           </tr>
                           <tr>
                              <td className='text-center'>
                                 {/* 
                                 <button className=" col-lg-3 BottomButtonOne ReferBottom mb-0">
                                    <img className="mr-2" src={ReferMan} alt="" />
                                    Referred to
                                   </button> */}
                                 <button type="submit" className="mb-2 mt-3 col-lg-3  BottomButtonTwo">
                                    <img className="mr-2" src={ReferManLight} alt="" />
                                            Reffer
                                 </button>

                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </form>
            </Modal.Body>
         </Modal>
      </Fragment >
   )
}

export default Refer
