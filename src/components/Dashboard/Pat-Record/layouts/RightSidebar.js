import React, { Fragment, useState } from 'react'

import Note from "../../icons/Note.svg";
import Bottle from "../../icons/Bottle.svg";
import Prescription from "../../icons/Prescription.svg";
import Clicp from "../../icons/Clicp.svg";
import { Pagination, Tab, Tabs } from 'react-bootstrap';
import FollowUp from './FollowUp';
import Refer from './Refer';


const RightSidebar = () => {

   const [lgShow, setLgShow] = useState(false);
   
   
   const [lgShowRef, setLgShowRef] = useState(false);
   
   
   

   
   
   return (
      
      <Fragment>
         {
            setLgShow ? <FollowUp setLgShow={setLgShow} lgShow={lgShow} /> : ''
         }
         {
            lgShowRef ?
               <Refer setLgShowRef={setLgShowRef} lgShowRef={lgShowRef} />

               : ''
         }
                  <div className="card mt-0">
                           <div className="card-body">
                              <p className="card-text ">
                                 <Tabs 
                     defaultActiveKey="home"
                                    transition={false}
                                     id="noanim-tab-example">
                     <Tab eventKey="home" title="Medical Record " >
                                       <p className="text-left">
                                          <div className="table-responsive">
                                             <table className="table table-shopping ">
                                                <thead className="">
                                                   <tr>
                                                      <th className="">
                                                         Appointment

                                                      </th>
                                                    
                                       
                                                  <th className="" onClick={() => setLgShowRef(preMode=> !preMode)}>Reffered</th>
                                       
                                       
                                                      <th className="">
                                                         Purpose
                                                      
                                                      </th>

                                                      <th className="">
                                                         Dignose
                                                               
                                                      </th>
                                                      <th onClick={()=> setLgShow(true)} className="">
                                                         Follow up
                                                      </th>
                                                      <th></th>
                                                      <th></th>
                                                      <th></th>
                                                      <th></th>
                                                
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>

                                                   
                                             
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>

                                                   
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>

                                                   
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>

                                                   
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>

                                                   
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>

                                                   
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>

                                                   
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>

                                                   
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>

                                                   
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>

                                                   
                                                   <tr>
                                                      <td>
                                                         24.03.2021
                                                      </td>
                                                      <td>ABC Clinic</td>
                                                      <td>Fever</td>
                                                      <td>Not any</td>
                                                      <td>21.03.2021</td>
                                                      
                                                      <td><img src={Prescription} alt=""/></td>
                                                      
                                                      <td><img src={Bottle} alt="" /></td>
                                                      <td><img src={Clicp} alt=""/></td>
                                                      <td>
                                                         <img src={Note} alt="" />
                                                      </td>
                                                     
                                                   </tr>
                                                </tbody>
                                             </table>
                                             {/* pagination  */}
                                             <tfoot className='float-right mt-4'>
                                                <tr>
                                                   {/* pagination  */}
                                    <td>
                                       <Pagination>
                                                 
                                                      <Pagination.Prev />
                                                      <Pagination.Item active>{1}</Pagination.Item>
                                                      <Pagination.Ellipsis />

                                               
                                                      <Pagination.Item >{12}</Pagination.Item>
                                                     

                                                    
                                                      <Pagination.Next />
                                                   
                                                   </Pagination></td>
                                                </tr>
                                             </tfoot>
                                          </div>
                                       </p>
                                    </Tab>
                                    <Tab eventKey="Symptom" title="Symptom"></Tab>
                                    <Tab eventKey="Notes" title="Notes">
                                       <p className="text-left">
                                          adf Lorem ipsum dolor sit amet, consectetur
                                          adipisicing elit. Totam, tempora aliquid fugiat
                                          laudantium explicabo voluptatibus eveniet vitae
                                          optio, at sed rem? Molestias quo aspernatur suscipit
                                          laboriosam, quae at nulla incidunt!
                                    </p>
                                    </Tab>

                                    <Tab eventKey="Prescription" title="Prescription">
                                       <p className="text-left">
                                          adf Lorem ipsum dolor sit amet, consectetur
                                          adipisicing elit. Totam, tempora aliquid fugiat
                                          laudantium explicabo voluptatibus eveniet vitae
                                          optio, at sed rem? Molestias quo aspernatur suscipit
                                          laboriosam, quae at nulla incidunt!
                                       </p>
                                    </Tab>

                                    <Tab eventKey="Lab test" title="Lab test">
                                       <p className="text-left">
                                          adf Lorem ipsum dolor sit amet, consectetur
                                          adipisicing elit. Totam, tempora aliquid fugiat
                                          laudantium explicabo voluptatibus eveniet vitae
                                          optio, at sed rem? Molestias quo aspernatur suscipit
                                          laboriosam, quae at nulla incidunt!
                                      </p>
                                    </Tab>
                                 </Tabs>
                              </p>
                           </div>
                        </div>
                     
      </Fragment>
   )
}

export default RightSidebar
