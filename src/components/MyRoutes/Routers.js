import React from 'react'
import { Route } from 'react-router-dom'
import Calendar from '../Dashboard/Calendar/Calendar'

import Dashboard from '../Dashboard/Dashboard'
import Profile from '../Dashboard/Doctor/Profile'
import Fee from '../Dashboard/Fee/Fee'
import Index from '../Dashboard/MemberShip/Index'
import PatRecord from '../Dashboard/Pat-Record/PatRecord'
import Prescription from '../Dashboard/Prescription/Prescription'
import Schedule from '../Dashboard/Schedule/Schedule'


function Routers() {
   return (
      <div>
         <Route exact path="/" component={Dashboard}></Route>
         <Route exact path='/pat-record' component={PatRecord} />
         <Route exact path='/calander' component={Calendar} />
         <Route exact path='/schedule' component={Schedule} />
         <Route exact path='/profile' component={Profile} />
         <Route exact path='/membership' component={Index} />
         <Route exact path='/fee' component={Fee} />
         <Route exact path='/prescription' component={Prescription} />
      </div>
   )
}

export default Routers
