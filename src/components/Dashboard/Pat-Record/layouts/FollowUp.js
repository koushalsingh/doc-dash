import React, { Fragment, useEffect, useState } from 'react'
import { Button, Modal } from 'react-bootstrap'
import './FollowUp.css'
import FollowUpButton from '../../icons/FollowUpButton.svg'
import CalendarIcon from '../../icons/CalanderDarkIcon.svg'
const FollowUp = ({ lgShow, setLgShow }) => {
   const [RemoveFace, setRemoveFace] = useState(true)
   const ChangeIcon = (e) => {
      setRemoveFace(true);
      e.currentTarget.type = "date"
   }

   return (
      <Fragment>
         <Modal
            size="md"
            show={lgShow}
            onHide={() => setLgShow(false)}
            onClick={() => setRemoveFace(false)}
            aria-labelledby="example-modal-sizes-title-md"
         >
            <Modal.Header closeButton>
               <Modal.Title id="example-modal-sizes-title-md">
                  <span className="ModalTitle">Follow Up</span>
               </Modal.Title>
            </Modal.Header>
            <Modal.Body className="pb-0">
               <form action="">
                  <div className="table-responsive">
                     <table className="table table-shopping mb-0">
                        <thead>
                           <tr>
                              <th></th>

                           </tr>
                        </thead>
                        <tbody>

                           <tr>

                              <td>
                                 <div style={{ width: '92%', marginLeft: '10px' }} className="mb-3">

                                    <label htmlFor="">Select A Date</label>
                                    <input

                                       onFocus={ChangeIcon}
                                       onBlur={(e) => (e.currentTarget.type = "text")}

                                       placeholder='e.g 15.03.2021' className='form-control inputStyle' />


                                    {
                                       !RemoveFace ? <img className='PleceHolderIcon' src={CalendarIcon} alt="" /> : ''
                                    }
                                 </div>


                                 <td className="pb-0 ">
                                    <label className="pb-0 mb-0">Pick the time slot</label>
                                 </td>
                                 <tr>
                                    <td className='d-flex justify-content-between' >
                                       <span className='ForTime'>10:00 AM - 11:00 AM</span>
                                       <span className='ForTime'>10:00 AM - 11:00 AM</span>
                                       <span className='ForTime'>10:00 AM - 11:00 AM</span>
                                    </td>
                                 </tr>
                                 <tr >
                                    <td className='pt-0'>
                                       <span className='ForTime'>10:00 AM - 11:00 AM</span>
                                       <span className='ForTime'>10:00 AM - 11:00 AM</span>
                                       <span className='ForTime'>10:00 AM - 11:00 AM</span>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td className='text-center pb-0 pt-4'>
                                       <button type="submit" className="mb-0 col-lg-4  BottomButtonTwo">
                                          <img className="mr-2" src={FollowUpButton} alt="" />
                                          Follow Up
                                          </button>
                                    </td>
                                 </tr>

                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </form>
            </Modal.Body>
         </Modal>
      </Fragment >
   )
}

export default FollowUp
