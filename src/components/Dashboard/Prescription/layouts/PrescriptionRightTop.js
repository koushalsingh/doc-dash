import React, { Fragment } from 'react'
import Appointment from '../../icons/Appointment.svg'
function PrescriptionRightTop() {
   return (
      <Fragment>
         <div className="card appointments mt-0 col-lg-12">
            <div className="card-body">
               <h5 className="card-title row pt-3 pb-3 text-left">
                  <div className=" pl-4">
                     <img src={Appointment} alt="" className="mr-2" />
                     <span className="appointmentText">Membership</span>
                  </div>
               </h5>

               <p className="card-text">

               </p>
            </div>
         </div>
      </Fragment>
   )
}

export default PrescriptionRightTop
