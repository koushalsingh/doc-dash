import React, { Fragment } from 'react'
import HeaderTop from '../Layouts/HeaderTop'
import LeftSidebar from '../Layouts/LeftSidebar'
import Appointment from '../icons/Appointment.svg'
import PrescriptionLeftSide from './layouts/PrescriptionLeftSide'

function Prescription() {
   return (
      <Fragment>
         <div className="wrapper">
            {/* Left Side Bar  */}
            <LeftSidebar />
            <div className="main-panel">
               {/* Header top Section  */}
               <HeaderTop searchBar={false} TimeBar={false} />

               {/* Main Content start Here  */}

               <div className="content mr-0 pr-0">
                  <div className="container-fluid row mt-4 pr-0 mr-0">

                     {/* Left side content  */}
                     <div className="col-lg-8">
                        <PrescriptionLeftSide />
                     </div>


                     {/* right side content  */}
                     <div className="col-lg-4">
                        {/* TOp are  */}
                        <div className="card appointments mt-0 col-lg-12">
                           <div className="card-body">
                              <h5 className="card-title row pt-3 pb-3 text-left">
                                 <div className=" pl-4">
                                    <img src={Appointment} alt="" className="mr-2" />
                                    <span className="appointmentText">Membership</span>
                                 </div>
                              </h5>

                              <p className="card-text">

                              </p>
                           </div>
                        </div>
                     </div>



                  </div>
               </div>
            </div>
         </div>

      </Fragment>
   )
}

export default Prescription
