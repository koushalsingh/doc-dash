import React, { Fragment } from 'react'
import { Modal } from 'react-bootstrap'
import styled from 'styled-components'
import RefeshIcon from '../../icons/RefeshIcon.svg'
const CustomRecurring = ({ lgShow, setLgShow }) => {
   return (
      <Fragment>

         <Modal
            size="md"
            show={lgShow}
            onHide={() => setLgShow(false)}
            aria-labelledby="example-modal-sizes-title-md"

         >
            <Modal.Header closeButton>
               <Modal.Title id="example-modal-sizes-title-md">
                  <span className="ModalTitle">
                     <img src={RefeshIcon} className='mr-2' alt="" />
                     Custom recurrence</span>
               </Modal.Title>
            </Modal.Header>
            <Modal.Body className="pb-0">
               <ContentArea>
                  <form action="">
                     <div className=" d-flex">
                        <label className=' col-lg-5 col-sm-5 pl-0 mt-1' htmlFor="">Repeat every </label>
                        <input type="number" className='CustomInput col-lg- col-sm-3 mr-2 ' />

                        <select name="" className='form-control col-lg-3 col-sm-3' id="">

                           <option value="">Weak</option>
                           <option value="">Weak</option>
                           <option value="">Weak</option>
                        </select>
                     </div>


                     {/* Repeat on  */}
                     <div className=" ArrowClass text-left mt-4 ">
                        <label className="" htmlFor="TimeSlot">
                           Repeat on</label>
                        <div className="row">


                           <div id='TimeSlot' className="daySlot col-lg-12  mt-2 mb-3">
                              <span className='DaySlotSpan' >S</span>
                              <span className='DaySlotSpan' >M</span>
                              <span className='DaySlotSpan active' >T</span>
                              <span className='DaySlotSpan' >W</span>
                              <span className='DaySlotSpan' >T</span>
                              <span className='DaySlotSpan' >F</span>
                              <span className='DaySlotSpan' >S</span>
                           </div>

                        </div>
                     </div>



                     {/* End section   */}
                     <div className=" ArrowClass text-left mt-4 ">
                        <label className="" htmlFor="TimeSlot">
                           End</label>
                        <div className="row">

                           <div className="col-lg-6">
                              <div className="row">
                                 <div className="col-lg-4 mb-3">
                                    <input name='crs' id='never' style={{ height: 18 }} type="radio" value='never' />
                                 </div>
                                 <div className="col-lg-8 pl-0">
                                    <label htmlFor='never' className='customScheduleHeader'>Never</label>
                                 </div>

                                 <div className="col-lg-4 mb-3">
                                    <input name='crs' id='on' style={{ height: 18 }} type="radio" value='on' />
                                 </div>
                                 <div className="col-lg-8 pl-0">
                                    <label htmlFor='on' className='customScheduleHeader' >On</label>
                                 </div>

                                 <div className="col-lg-4 mb-3">
                                    <input name='crs' id='after' value='after' style={{ height: 18 }} type="radio" />
                                 </div>
                                 <div className="col-lg-8 pl-0">
                                    <label className='customScheduleHeader' htmlFor='after'>After</label>
                                 </div>
                              </div>
                           </div>


                           <div className="col-lg-6">
                              <div className="row">
                                 <div className="col-lg-12 pl-0 mt-4 mb-2">
                                    <input type="date" className="form-control" />

                                 </div>
                                 <div className="col-lg-12  pl-0">
                                    <input type="text" className="form-control" />
                                 </div>
                              </div>
                           </div>


                        </div>
                        <div className="row float-right mb-4 mt-4 mr-2">
                           <button className='CencleButton' onClick={() => setLgShow(false)}>Cancle</button>
                           <button type='submit' className='DoneButton'>Done</button>
                        </div>
                     </div>




                  </form>
               </ContentArea>
            </Modal.Body>
         </Modal>


      </Fragment>
   )
}

const ContentArea = styled.div`
 
/* basic layout */
fieldset {
  margin: 20px;
  max-width: 400px;
}
label > input[type="radio"] + * {
  display: inline-block;
  padding: 0.5rem 1rem;
}
.DoneButton{
    color: #C06C84;
    background: transparent;
    font-size: 16px;
    font-weight: 500;
    letter-spacing: .5px;
    margin-left: 7px;
}
.CencleButton{
    background: transparent;
    font-size: 16px;
    font-weight: 500;
    letter-spacing: .5px;
    margin-left: 7px;
    color:#1C1C1C;
}

.customScheduleHeader{
      font-size: 14px;
    font-family: 'Poppins';
    font-weight: 500;
    text-transform: capitalize;
  
    color: #1C1C1C;
}
/* Slot Active class  */
.active{
     background:#C06C84!important;
       color:#fff!important;
    
     
}
/* DaySlot  */
.DaySlotSpan{
    
    color: #707070;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 12px;
    padding-right: 12px;
    border-radius: 33px;
    margin-right: 14px;
    font-family: 'Poppins'!important;
    font-size: 13px!important;
    font-weight: 400;
    -webkit-transition-duration: 400ms;
    transition-duration: 400ms;
    background:#F1F3F4;
    :hover{
       background:#C06C84;
       color:#fff;
       transition-duration:400ms;
     
    }
}

/* Input Style  */
input {
    border: 1px solid #d2d2d2!important;
    border-radius: 6px;
}

`

export default CustomRecurring
